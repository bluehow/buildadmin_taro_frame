export default {
    id: 'ID',
    nickname: '微信昵称',
    update_time: '修改时间',
    openid: 'OpenId',
    unionid: 'UnionId',
    lasttime: '上次访问时间',
    create_time: '第一次访问时间',
    'quick Search Fields': '微信昵称',
}
