<?php
declare (strict_types = 1);

namespace app\api\middleware;

use app\common\facade\Token;
use think\Response;
use think\Config;
use think\exception\HttpResponseException;

class VistorAuth
{

    protected $config = [];
     /**
     * VistorAuth constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config  = $config->get('mobile');
    }

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        if($request->header('platform')=='wxapp'){
            //微信小程序
            if($request->header('session_key')){
                $user = Token::get('miniapp:'.$request->header('session_key'),false);
                if(!$user){
                    $response = Response::create(['msg'=>'请登录','time'=>$request->server('REQUEST_TIME'),'data'=>[],'code'=>1,],'json',$this->config['need_log_code']);
                    return new HttpResponseException($response);
                }
                $request->user = $user;
            }else {
                $response = Response::create(['msg'=>'缺少session_key','time'=>$request->server('REQUEST_TIME'),'data'=>[],'code'=>0],'json');
                return new HttpResponseException($response);
            }
        }else if ($request->header('platform')=='h5'){
            //微信公众号网页
            // TODO: 完成微信公众号网页端的鉴权
        }
        $response = $next($request);
        return $response->header(['MOBILE_CONFIG_VISION'=>$this->config['vision']]);
    }
}
