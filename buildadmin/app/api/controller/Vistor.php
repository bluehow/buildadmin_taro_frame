<?php

namespace app\api\controller;

use app\common\controller\Api;

class Vistor extends Api
{
    protected $middleware = ['VistorAuth'];
}