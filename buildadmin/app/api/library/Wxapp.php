<?php

namespace app\api\library;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
class Wxapp {

    public string $appId;

    public string $appSecret;

    // 小程序登录
    public function login(string $js_code):bool|array
    {
        $request_url = "https://api.weixin.qq.com/sns/jscode2session";
        $request_method = "GET";
        $request_body = [
            'appid'=>$this->appId,
            'secret'=>$this->appSecret,
            'js_code'=>$js_code,
            'grant_type'=>'authorization_code',
        ];
        $request = new Request($request_method,$request_url . '?' . http_build_query($request_body));
        $client = new Client();
        $response = $client->send($request);
        $code = $response->getStatusCode();
        if($code == 200){
            $body = $response->getBody();
            $data = json_decode($body,true);
            return $data;
        }else{
            return false;
        }
    }
}