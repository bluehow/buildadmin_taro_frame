<?php

namespace app\admin\controller\app;

use app\common\controller\Backend;

/**
 * 访客管理
 */
class Visitor extends Backend
{
    /**
     * Visitor模型对象
     * @var object
     * @phpstan-var \app\admin\model\app\Visitor
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'update_time', 'create_time'];

    protected string|array $quickSearchField = ['nickname'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\app\Visitor;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */
}