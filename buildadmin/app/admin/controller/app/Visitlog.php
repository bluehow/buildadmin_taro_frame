<?php

namespace app\admin\controller\app;

use app\common\controller\Backend;

/**
 * 访问记录管理
 */
class Visitlog extends Backend
{
    /**
     * Visitlog模型对象
     * @var object
     * @phpstan-var \app\admin\model\app\Visitlog
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time'];

    protected string|array $quickSearchField = ['vistor.nickname'];
    protected array $withJoinTable = ['vistor'];

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\app\Visitlog;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */
}