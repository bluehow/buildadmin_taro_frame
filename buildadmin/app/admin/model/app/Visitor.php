<?php

namespace app\admin\model\app;

use think\Model;

/**
 * Visitor
 */
class Visitor extends Model
{
    // 表名
    protected $name = 'app_visitor';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 字段类型转换
    protected $type = [
        'lasttime' => 'timestamp:Y-m-d H:i:s',
    ];

    protected static function onBeforeInsert($model)
    {
        $pk         = $model->getPk();
        $model->$pk = \app\common\library\SnowFlake::generateParticle();
    }

    public function getIdAttr($value): string
    {
        return (string)$value;
    }
}