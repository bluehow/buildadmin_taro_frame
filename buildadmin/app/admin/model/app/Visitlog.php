<?php

namespace app\admin\model\app;

use think\Model;

/**
 * Visitlog
 */
class Visitlog extends Model
{
    // 表名
    protected $name = 'app_visitlog';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

    // 追加属性
    protected $append = [

    ];

    public function vistor(){
        return $this->belongsTo(Visitor::class,'app_visitor_ids','id');
    }
}